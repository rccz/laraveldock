# Laravel Docker Container
This repo contains the configuration for Laravel application development docker containers using MySql and Nginx.

## Preinstallation
##### Clone the repo
```
git clone git@bitbucket.org:rccz/laraveldock.git
```
##### Change .env variable values as your new application needs
The repo is prepared to run the initial configuration, only the specific values must be changed:

* APP_NAME
* PHP_PORT
* WEB_PORT
* MYSQL_PORT
* MYSQL_ROOT_PASSWORD
* MYSQL_DATABASE
* MYSQL_USER
* MYSQL_PASSWORD
* TZ (mysql server timezone)
* MYSQL_IMPORT_DATA_FILE (filename + extension)

## Installation
```
docker-compose up -d
```
###### *Permissions issues*
*If you can't edit files, run the following command in your terminal:*
```
sudo chmod a+rwx -R src/ && sudo chmod a+rwx -R data/
```
## PostInstall - Existing App Source

###### Modify permissions
```
docker-compose exec php-fpm sh /config/set_permissions.sh
```

###### Database data import
If you already have an exported .sql file, this file must be located at the root of your project and the name changed at .env file


```
docker-compose exec mysql sh /config/import_data.sh
```

## PostInstall - New App Source

```
docker-compose exec php-fpm sh /config/create_app.sh
```

